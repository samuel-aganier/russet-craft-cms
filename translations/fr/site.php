<?php

return array(


	"Send"=>"Soumettre",
	"Full Name"=>"Nom complet",
	"Phone number"=>"Numéro de téléphone",
	"Email"=>"Adresse courriel",
	"Contact information"=>"Coordonnées",
	"Attach your curriculum vitae"=>"Veuillez joindre votre curriculum vitae",
	"Attach your cover letter"=>"Veuillez joindre votre lettre de présentation",
	"Cover letter"=>"Lettre de présentation",

	"All rights reserved"=>"Tous droits réservés",
	"Your message has been sent successfully. We will contact you if your profile matches one of our positions."=>"Votre message a été envoyé avec succès. Nous vous contacterons si votre profil correspond à l’un de nos postes.",
	"Join our team"=>"Joignez-vous à l'équipe",
	"Contact us"=>"Nous joindre", 
	"Visit us"=>"Visitez-nous",
	"All rights reserved"=>"Tous droits réservés",
	"Signed Zel"=>"Signé Zel",
	"available jobs"=>"Postes disponibles",
	"Job description"=>"Description du poste",
	"Qualifications & knowledge"=>"Qualifications & connaissances",
	"Benefits"=>"Avantages",
	"Apply now"=>"Postulez maintenant",
	"All"=>"Tous",
	"Our projects"=>"Nos projets",
	"Contact us"=>"Contactez-nous",
	"Follow us"=>"Suivez-nous",

	"Adress"=>"Adresse",
	"This field is required."=>"Ce champ est obligatoire.",

	// Russet
	"Competitive advantages"=>"Des avantages concurrentiels",
	"Home"=>"Accueil",
	"Change language"=>"Changer de langue",
	"Return to products"=>"Retournez à la  liste de produits",
	"Contact our sales team"=>"Contactez notre équipe de ventes",
	"Two service points"=>"Deux points de service",
	"Head office"=>"Bureau principal",
	"Sales office"=>"Bureau des ventes",
	"Contact a member of our team"=>"Contactez un membre de notre équipe",
	"Discover our job offers"=>"Découvrez nos offres d'emploi",
	
	"
        142, road 202, # 203
        Huntingdon (QC)
        Canada J0S 1H0"=>"
        142, route 202, # 203
        Huntingdon (QC)
        Canada J0S 1H0",

	"Please note that we will only communicate with the people who will be selected for an interview. You can also go in person to the following address:"=>"Veuillez noter que nous communiquerons uniquement avec les personnes qui seront sélectionnées en entrevue. Vous pouvez également vous présenter en personne a l’adresse suivante:",
		"Learn more"=>"En savoir plus",
	"Please fill out the form below for more information about our products, private label and customization of your own product, sales, logistics and our R & D department, or for any sponsorship requests."=>"Veuillez remplir le formulaire ci-dessous pour plus de renseignements sur nos produits, la marque privée et la personnalisation de votre propre produit, la vente, la logistique et notre département R&D, ou pour toutes demandes de commandite."

	





);