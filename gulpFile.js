'use strict';

var gulp = require('gulp'),
    browser = require('browser-sync').create(),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minijyJS = require('gulp-uglify'),
    concatJS = require('gulp-concat');

gulp.task('003', function() {
    gulp.watch('public_html/assets/css/**/*.scss',['sass']);
    gulp.watch('public_html/assets/js/vendor/*.js', ['js'])
    });


gulp.task('html', function() {
    return gulp
        .src('templates/*.html')
        .pipe(gulp.dest('dist'));
});


gulp.task('sass', function() {
    return gulp
    .src('public_html/assets/css/**/*.scss')
    .pipe(sass({
outputStyle: 'condensed'
        }))
    .pipe(autoprefixer({
        browsers: ['last 2 versions', 'ie 6-14']
        }))
    .pipe(gulp.dest('public_html/assets/css/'));
    })


gulp.task('js', function() {
    return gulp
    .src('public_html/assets/js/vendor/*.js')
    .pipe(concatJS('scripts.js'))
    .pipe(minijyJS())
    .pipe(gulp.dest('public_html/assets/js/'));
    })


gulp.task('default', ['sass','js','003']);      