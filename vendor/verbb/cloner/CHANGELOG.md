# Changelog

## 1.0.3 - 2019-05-18

### Fixed
- Fix error when special characters are in the name for cloned items.

## 1.0.2 - 2019-03-13

### Fixed
- Fix cloner button not entry types screen when only one entry type exists.

## 1.0.1 - 2018-11-28

### Fixed
- Fixed an issue where registering plugin resources caused other plugins to fail catastrophically.

## 1.0.0 - 2018-11-19

- Initial release.
